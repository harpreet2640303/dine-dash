import React, { useState, useEffect } from "react";
import "./overlay.css";

function Overlay({
  // selectedRestaurant: { restaurant },
  availableSlots,
  trigger,
  setTrigger,
  selectedDay,
  setSelectedDay,
  selectedDate,
  setSelectedDate,
  selectedStartTime,
  setSelectedStartTime,
  selectedEndTime,
  setSelectedEndTime,
  selectedGuest,
  setSelectedGuest,
  bookingCounts,
  setBookingCounts,
  selectedSlot,
  setSelectedSlot,
}) {
  const [selectedDateIndex, setSelectedDateIndex] = useState(0);

  const today = new Date();
  const todayDates = Array.from({ length: 10 }, (_, index) => {
    const date = new Date(today);
    date.setDate(today.getDate() + index);
    return date;
  });

  // useEffect(() => {
  //   // Set default selected date and time when the component mounts
  //   handleDateSelection(selectedDateIndex);
  //   // handleSlotSelection(availableSlots[0]); // Default to the first slot
  // }, []); // Empty dependency array to run the effect once on mount

  const formatDate = (dateObject) => {
    const day = dateObject.getDate();
    const formattedDay = day < 10 ? `0${day}` : `${day}`;
    const weekday = dateObject.toLocaleDateString("en-US", {
      weekday: "long",
    });
    const monthAbbreviation = dateObject.toLocaleString("default", {
      month: "short",
    });

    return `${weekday}, ${formattedDay}-${monthAbbreviation}`;
    // return `${formattedDay}-${monthAbbreviation}`;
  };

  const getFormattedDay = (dateObject) => {
    const day = dateObject.getDate();
    // const formattedDay = day < 10 ? `0${day}` : `${day}`;
    const weekday = dateObject.toLocaleDateString("en-US", {
      weekday: "short",
    });
    // const monthAbbreviation = dateObject.toLocaleString("default", {
    //   month: "short",
    // });

    // return `${weekday}, ${formattedDay}-${monthAbbreviation}`;
    return `${weekday}`;
  };

  const getFormattedDate = (dateObject) => {
    const day = dateObject.getDate();
    const formattedDay = day < 10 ? `0${day}` : `${day}`;
    // const weekday = dateObject.toLocaleDateString("en-US", {
    //   weekday: "short",
    // });
    const monthAbbreviation = dateObject.toLocaleString("default", {
      month: "short",
    });

    // return `${weekday}, ${formattedDay}-${monthAbbreviation}`;
    return `${formattedDay}-${monthAbbreviation}`;
  };

  const handleDateSelection = (index) => {
    console.log(index);
    setSelectedDateIndex(index);
    // Get the selected date
    const selectedDateObject = todayDates[index];
    console.log(selectedDateObject);

    // Format the selected date as a string
    // const formattedDate = selectedDateObject.toISOString();
    // const formattedDate = formatDate(selectedDateObject);
    // console.log(formattedDate);

    // Set the selectedDate state
    setSelectedDate(getFormattedDate(selectedDateObject));
    // console.log(selectedDate);

    // Set the selectedDay state
    setSelectedDay(getFormattedDay(selectedDateObject));
    // console.log(selectedDay);
  };

  // const getFormattedDay = (dateObject) => {
  //   // console.log(dateObject)
  //   const today = new Date();
  //   const tomorrow = new Date(today);
  //   tomorrow.setDate(today.getDate() + 1);

  //   if (dateObject.toDateString() === today.toDateString()) {
  //     return "Today";
  //   } else if (dateObject.toDateString() === tomorrow.toDateString()) {
  //     return "Tomorrow";
  //   } else {
  //     const day = dateObject.getDate();
  //     const formattedDay = day < 10 ? `0${day}` : `${day}`;
  //     const weekday = dateObject.toLocaleDateString("en-US", {
  //       weekday: "short",
  //     });
  //     const monthAbbreviation = dateObject.toLocaleString("default", {
  //       month: "short",
  //     });

  //     // console.log(weekday, formattedDay, monthAbbreviation)
  //     return `${weekday}, ${formattedDay} ${monthAbbreviation}`;
  //   }
  // };

  // const isSlotFull = () => {
  //   if (selectedSlot) {
  //     // Get the booking count for the selected slot
  //     const bookingCount = bookingCounts[selectedSlot.id] || 0;

  //     // Compare with the capacity
  //     return bookingCount >= selectedSlot.capacity;
  //   }

  //   // Return false if no slot is selected
  //   return false;
  // };

  const isSlotCapacityFull = (slot) => {
    const bookingCount = bookingCounts[slot.id] || 0;
    return bookingCount >= slot.capacity;
  };

  const handleSlotSelection = (selectedSlot) => {
    setSelectedSlot(selectedSlot);

    if (!isSlotCapacityFull(selectedSlot)) {
      const { start_time, end_time } = selectedSlot;

      setSelectedStartTime((prevStartTime) =>
        start_time !== prevStartTime ? start_time : prevStartTime
      );
      setSelectedEndTime((prevEndTime) =>
        end_time !== prevEndTime ? end_time : prevEndTime
      );

      console.log("Selected Slot:", selectedSlot);
    } else {
      console.log("Selected Slot is Full:", selectedSlot);
    }
  };

  return trigger ? (
    <div className="overlay">
      <div className="overlay-inner">
        <div className="overlayTitle-box">
          <h2 className="overlay-title">Select Slot & Guests</h2>
          <button className="overlay-btn" onClick={() => setTrigger(false)}>
            <ion-icon name="close-circle-outline"></ion-icon>
          </button>
        </div>

        <div className="overlayList-box">
          <div className="overlayList-content">
            <h3 className="overlayList-title">Date</h3>
            <div className="overlayList-container">
              <div className="overlay-list">
                {todayDates.map((date, index) => (
                  <button
                    key={index}
                    type="button"
                    onClick={() => handleDateSelection(index)}
                    className={`overlayList-item ${
                      selectedDateIndex === index ? "selected" : ""
                    }`}
                  >
                    <span className="overlayList-item--slot">
                      {formatDate(date)}
                    </span>
                  </button>
                ))}
              </div>
            </div>
          </div>

          <div className="overlayList-content">
            <h3 className="overlayList-title">Dining Time</h3>
            <div className="overlayList-container">
              <div className="overlay-list">
                {availableSlots.map((slot, index) => (
                  <button
                    key={index}
                    type="button"
                    onClick={() => handleSlotSelection(slot)}
                    className={`overlayList-item ${
                      // isSlotCapacityFull(slot) ? "full" : ""
                      slot.start_time === selectedStartTime &&
                      slot.end_time === selectedEndTime
                        ? "selected"
                        : ""
                    }`}
                  >
                    {`${slot.start_time} - ${slot.end_time}`}
                  </button>
                ))}
              </div>
            </div>
          </div>

          <div className="overlayList-content">
            <h3 className="overlayList-title">How Many Guests?</h3>
            <div className="overlayList-container">
              <div className="overlay-list">
                {[...Array(10).keys()].map((index) => (
                  <button
                    key={index}
                    type="button"
                    onClick={() => setSelectedGuest(index + 1)}
                    className={`overlayList-item ${
                      selectedGuest === index + 1 ? "selected" : ""
                    }`}
                  >
                    {index + 1}
                  </button>
                ))}
              </div>
            </div>
          </div>
          <button
            className="btn btn--search btn--overlay"
            onClick={() => setTrigger(false)}
          >
            Find best offer
          </button>
        </div>
      </div>
    </div>
  ) : null;
}

export default Overlay;
