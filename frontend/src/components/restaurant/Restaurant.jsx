import React from "react";
import "./restaurant.css";
import { useNavigate } from "react-router-dom";

export default function Restaurant({ item: restaurant }) {
  const navigate = useNavigate();

  function handleClick() {
    navigate(`/restaurant/${restaurant.id}`, { state: { restaurant } });
  }

  return (
    <div className="restaurant">
      <div onClick={handleClick} className="restaurantImg__box">
        <img className="restaurant__img" src={restaurant.image} alt="" />
      </div>

      <div className="restaurantText__box">
        <div className="restaurant__location">
          <ion-icon class="location__icon" name="location-outline"></ion-icon>
          <h5 className="restaurant__name">{restaurant.location}</h5>
        </div>

        <h2 className="restaurant__title">{restaurant.name}</h2>
        <h6 className="restaurant__type">{restaurant.cuisine_type}</h6>
        <p className="restaurant__description">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut .
        </p>
      </div>
    </div>
  );
}
