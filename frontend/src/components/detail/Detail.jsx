import React, { useState, useEffect } from "react";
import "./detail.css";
import Overlay from "../overlay/Overlay";
import { useNavigate } from "react-router-dom";

const Detail = ({
  selectedRestaurant,
  availableSlots,
  selectedDay,
  setSelectedDay,
  selectedDate,
  setSelectedDate,
  selectedStartTime,
  setSelectedStartTime,
  selectedEndTime,
  setSelectedEndTime,
  selectedGuest,
  setSelectedGuest,
  bookingCounts,
  setBookingCounts,
  selectedSlot,
  setSelectedSlot,
}) => {
  const [buttonPopup, setButtonPopup] = useState(false);
  const [trigger, setTrigger] = useState(false);
  const navigate = useNavigate();

  // console.log(selectedStartTime);
  // console.log(selectedRestaurant);

  function handleBookingClick() {
    // if (selectedDate && selectedStartTime && ) {
    navigate("/booking", {
      state: {
        selectedRestaurant,
        selectedDate,
        selectedDay,
        selectedStartTime,
        selectedEndTime,
        selectedGuest,
      },
    });
  }

  console.log(selectedDay);
  console.log(selectedDate);

  return (
    <div className="container grid grid--2-cols margin-bottom-md">
      <div className="detail">
        <img
          src={selectedRestaurant.image}
          className="detail-img"
          alt="Restaurant Pic"
        />
        <div className="detail-content">
          <p className="detail-title">{selectedRestaurant.name}</p>
          <ul className="detail-attributes">
            <li className="detail-attribute">
              <ion-icon class="detail-icon" name="location-outline"></ion-icon>
              <span>{selectedRestaurant.location}</span>
            </li>
            <li className="detail-attribute">
              <ion-icon class="detail-icon" name="cash-outline"></ion-icon>
              <span>
                1100 for two approx | {selectedRestaurant.cuisine_type}
              </span>
            </li>
            <li className="detail-attribute">
              <ion-icon class="detail-icon" name="time-outline"></ion-icon>
              <span>Open from 9:00 AM - 01:00 AM</span>
            </li>
          </ul>
        </div>
      </div>

      <div className="detail slot">
        <p className="slot-title">Select Slot & Guests</p>
        <div onClick={() => setButtonPopup(true)} className="slotPicker">
          <div className="slot-item date_slot">
            <span>
              <ion-icon class="detail-icon" name="calendar-outline"></ion-icon>
            </span>
            <span className="slot_time">
              {selectedDate}, {selectedStartTime}
            </span>
          </div>
          <div className="divider"></div>
          <div
            // onClick={() => setButtonPopup(true)}
            className="slot-item guest_slot"
          >
            <span>
              <ion-icon class="detail-icon" name="people-outline"></ion-icon>
            </span>
            <span>{selectedGuest} Guests</span>

            <ion-icon
              class="detail-icon down-arrow"
              name="chevron-down-outline"
            ></ion-icon>
          </div>
        </div>

        <button
          onClick={handleBookingClick}
          className="btn btn--search btn--book"
        >
          Book Now
        </button>
      </div>
      <Overlay
        availableSlots={availableSlots}
        trigger={buttonPopup}
        setTrigger={setButtonPopup}
        selectedDay={selectedDay}
        setSelectedDay={setSelectedDay}
        selectedDate={selectedDate}
        setSelectedDate={setSelectedDate}
        setSelectedStartTime={setSelectedStartTime}
        setSelectedEndTime={setSelectedEndTime}
        setSelectedGuest={setSelectedGuest}
        selectedGuest={selectedGuest}
        bookingCounts={bookingCounts}
        setBookingCounts={setBookingCounts}
        selectedSlot={selectedSlot}
        setSelectedSlot={setSelectedSlot}
        // bookingCounts={bookingCounts}
      ></Overlay>
    </div>
  );
};

export default Detail;
