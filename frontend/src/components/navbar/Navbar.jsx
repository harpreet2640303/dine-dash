import React from "react";
import Search from "../search/Search";
import logo from "../../assets/Dine Dash-logos/Dine Dash-logos_black.png";
import "./navbar.css";
import { Link } from "react-router-dom";
import { useState } from "react";

const Navbar = ({ setSearchText }) => {
  const [isNavOpen, setNavOpen] = useState(false);

  function handleClick() {
    setNavOpen(!isNavOpen);
  }

  function handleClickLogout() {
    localStorage.removeItem("authToken");
  }

  return (
    <header className={`header ${isNavOpen ? "nav-open sticky" : ""}`}>
      <Link to="#">
        <img className="logo" alt="DineDash logo" src={logo} />
      </Link>
      <Search setSearchText={setSearchText} />

      <nav className="navbar">
        <ul className="navbar-list">
          <li>
            <Link className="navbar-link" to="#how">
              Contact
            </Link>
          </li>
          <li>
            {/* <Link className="navbar-link" to="/login">
              {localStorage.getItem('authToken') ? "Log" : }
            </Link> */}
            {!localStorage.getItem("authToken") ? (
              <Link className="navbar-link" to="/login">
                Login
              </Link>
            ) : (
              <Link
                className="navbar-link"
                to="/"
                onClick={handleClickLogout}
              >
                Log out
              </Link>
            )}
          </li>
          <li>
            {/* <Link className="navbar-link nav-cta" to="/signup">
              Sign Up
            </Link> */}
            {!localStorage.getItem("authToken") ? (
              <Link className="navbar-link nav-cta" to="/signup">
                Sign up
              </Link>
            ) : null}
          </li>
        </ul>
      </nav>

      <button onClick={handleClick} className="btn-mobile-nav">
        <ion-icon class="icon-mobile-nav" name="menu-outline"></ion-icon>
        <ion-icon class="icon-mobile-nav" name="close-outline"></ion-icon>
      </button>
    </header>
  );
};

export default Navbar;
