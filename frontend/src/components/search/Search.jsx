import React from "react";
import "./search.css";
import { useState } from "react";
import { getFilteredRestaurants } from "../../helpers/restaurantHelpers";

const Search = ({ setSearchText }) => {
  // const [searchItem, setSearchItem] = useState("");

  function handleChange(e) {
    const searchText = e.target.value;
    // setSearchItem(searchText);
    setSearchText(searchText);
  }

  // function handleSearch() {
  //   const filteredRestaurants = getFilteredRestaurants(searchItem);
  //   updateShowRestaurants(filteredRestaurants);
  // }

  return (
    <div className="navbarSearch">
      <div className="navbarSearchItem">
        <ion-icon name="restaurant-outline"></ion-icon>
        <input
          type="text"
          placeholder="Search for location or cuisines"
          className="navbarSearchInput"
          onChange={handleChange}
        />
      </div>
      {/* <div className="navbarSearchItem">
        <button className="btn btn--search">
          <ion-icon name="search-outline"></ion-icon>
          <span className="searchText">Search</span>
        </button>
      </div> */}
    </div>
  );
};

export default Search;
