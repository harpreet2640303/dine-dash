import React, { useEffect, useState } from "react";
import "./loginForm.css";
import loginImg from "../../assets/img/bg-image3.jpg";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";

export default function LogininForm() {
  const navigate = useNavigate();
  const [values, setValues] = useState({
    // name: "",
    email: "",
    password: "",
  });

  useEffect(() => {
    const authToken = localStorage.getItem("authToken");
    console.log("Yay i'm checking authToken")
    if (authToken) {
      navigate("/");
    }
  }, [navigate]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await axios.post("http://localhost:8082/auth/login", values, {
        withCredentials: true,
      });

      const receivedToken = res.data.token;
      console.log(receivedToken);
      localStorage.setItem("authToken", receivedToken);
      navigate("/");
      console.log(res);
    } catch (err) {
      console.error(err.response.data);
    }
  };

  return (
    <>
      <div className="login">
        <img src={loginImg} alt="login-img" className="login__bg" />

        <form
          action="/auth/login"
          method="POST"
          className="login__form"
          onSubmit={handleSubmit}
        >
          <h1 className="login__title">Login</h1>

          <div className="login__inputs">
            {/* <div className="login__box">
              <input
                onChange={handleInput}
                type="name"
                placeholder="Name"
                required
                className="login__input"
                name="name"
                id="name"
              />
              <ion-icon class="detail-icon login-icon" name="person"></ion-icon>
            </div> */}

            <div className="login__box">
              <input
                type="email"
                placeholder="Email"
                required
                className="login__input"
                name="email"
                id="email"
                onChange={(e) =>
                  setValues({ ...values, email: e.target.value })
                }
              />
              <ion-icon class="detail-icon login-icon" name="mail"></ion-icon>
            </div>

            <div className="login__box">
              <input
                type="password"
                placeholder="Password"
                required
                className="login__input"
                name="password"
                id="password"
                onChange={(e) =>
                  setValues({ ...values, password: e.target.value })
                }
              />
              <ion-icon
                class="detail-icon login-icon"
                name="lock-closed"
              ></ion-icon>
            </div>
          </div>

          <div className="login__check">
            <div className="login__check-box">
              <input
                type="checkbox"
                className="login__check-input"
                id="user-check"
                name="user-check"
              />
              <label htmlFor="user-check" className="login__check-label">
                Remember me
              </label>
            </div>

            <Link to="#" className="login__forgot">
              Forgot Password?
            </Link>
          </div>

          <button
            type="submit"
            onSubmit={handleSubmit}
            className="btn 
          login__button"
          >
            Login
          </button>

          <div className="login__register">
            <div>Don't have an account?</div>
            <Link to="/signup">Create an account</Link>
          </div>
        </form>
      </div>
    </>
  );
}
