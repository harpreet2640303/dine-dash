import React, { useRef, useState, useEffect } from "react";
import "./signupForm.css";
import signinImg from "../../assets/img/bg-image3.jpg";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";

export default function SigninForm() {
  const navigate = useNavigate();

  const [values, setValues] = useState({
    name: "",
    email: "",
    password: "",
  });

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(values);
    try {
      const res = await axios.post(
        "http://localhost:8082/auth/register",
        values,
        {
          withCredentials: true,
        }
      );

      navigate("/login");
      console.log(res);
    } catch (err) {
      console.error(err.response.data);
    }
  };

  // const handleSubmit = async (e) => {
  //   e.preventDefault();
  //   axios
  //     .post("http://localhost:8082/auth/register", values, {
  //       withCredentials: true,
  //     })
  //     .then((res) => console.log(res))
  //     .then((err) => console.log(err));

  //   navigate("/login");
  // };

  return (
    <>
      <div className="login">
        <img src={signinImg} alt="background-img" className="login__bg" />

        <form
          action="/auth/register"
          method="POST"
          className="login__form"
          onSubmit={handleSubmit}
        >
          <h1 className="login__title">Sign Up</h1>

          <div className="login__inputs">
            <div className="login__box">
              <input
                type="name"
                placeholder="Name"
                required
                className="login__input"
                name="name"
                id="name"
                onChange={(e) => setValues({ ...values, name: e.target.value })}
              />
              <ion-icon class="detail-icon login-icon" name="person"></ion-icon>
            </div>

            <div className="login__box">
              <input
                type="email"
                placeholder="Email ID"
                required
                className="login__input"
                name="email"
                id="email"
                onChange={(e) =>
                  setValues({ ...values, email: e.target.value })
                }
              />
              <ion-icon class="detail-icon login-icon" name="mail"></ion-icon>
            </div>

            <div className="login__box">
              <input
                type="password"
                placeholder="Password"
                required
                className="login__input"
                name="password"
                id="password"
                onChange={(e) =>
                  setValues({ ...values, password: e.target.value })
                }
              />
              <ion-icon
                class="detail-icon login-icon"
                name="lock-closed"
              ></ion-icon>
            </div>
          </div>

          <button type="submit" className="login__button">
            Sign Up
          </button>

          <div className="login__register">
            <div>Already have an account?</div>
            <Link to="/login">Login</Link>
          </div>
        </form>
      </div>
    </>
  );
}
