import React from "react";
import "./summary.css";
import { Link, useLocation, useNavigate } from "react-router-dom";

export default function Summary({ restaurantInfo }) {
  const navigate = useNavigate();
  console.log(restaurantInfo);

  const handleEditClick = () => {
    console.log("HOLA");
    navigate(`/restaurant/${restaurantInfo.selectedRestaurant.id}`, {
      state: {
        restaurant: restaurantInfo.selectedRestaurant,
        selectedDate: restaurantInfo.selectedDate,
        selectedStartTime: restaurantInfo.selectedStartTime,
        selectedEndTime: restaurantInfo.selectedEndTime,
        selectedGuest: restaurantInfo.selectedGuest,
      },
    });
  };

  const handleBookClick = () => {
    navigate("/confirmation", {
      state: restaurantInfo,
    });
  };

  return (
    <div className="container--summary flex">
      <div className="detail detail-content summary">
        <div className="summaryTitle-box">
          <p className="summary-title">Booking Summary</p>
          {/* <Link
            to={`/restaurant/${state.restaurant.id}`}
            className="summary-edit"
          >
            Edit
          </Link> */}
          <button className="summary-edit" onClick={handleEditClick}>
            Edit
          </button>
        </div>
        <ul className="detail-attributes">
          <li className="detail-attribute">
            <ion-icon class="detail-icon" name="location-outline"></ion-icon>
            <span>{restaurantInfo.selectedRestaurant.location}</span>
          </li>
          <li className="detail-attribute">
            <ion-icon
              class="detail-icon"
              name="calendar-number-outline"
            ></ion-icon>
            <span>{restaurantInfo.selectedDate}</span>
          </li>
          <li className="detail-attribute">
            <ion-icon class="detail-icon" name="time-outline"></ion-icon>
            <span>{restaurantInfo.selectedStartTime}</span>
          </li>
          <li className="detail-attribute">
            <ion-icon class="detail-icon" name="people-outline"></ion-icon>
            <span>{`${restaurantInfo.selectedGuest} Guests`}</span>
          </li>
        </ul>
        <button
          onClick={handleBookClick}
          className="btn btn--search btn--overlay btn--summary"
        >
          Submit
        </button>
      </div>
    </div>
  );
}
