import React from "react";
import "./confirm.css";
import { Link, useLocation } from "react-router-dom";

export default function Confirm({ restaurantInfo }) {
  console.log(restaurantInfo);

  return (
    <div className="container--summary flex">
      <div className="detail detail-content summary">
        <div className="summaryTitle-box">
          <p className="summary-title">Confirmation Details</p>
        </div>
        <ul className="detail-attributes">
          <li className="detail-attribute">
            <ion-icon class="detail-icon" name="location-outline"></ion-icon>
            <span>{restaurantInfo.selectedRestaurant.location}</span>
          </li>
          <li className="detail-attribute">
            <ion-icon
              class="detail-icon"
              name="calendar-number-outline"
            ></ion-icon>
            <span>
              {restaurantInfo.selectedDay}, {restaurantInfo.selectedDate}
            </span>
          </li>
          <li className="detail-attribute">
            <ion-icon class="detail-icon" name="time-outline"></ion-icon>
            <span>{restaurantInfo.selectedStartTime}</span>
          </li>
          <li className="detail-attribute">
            <ion-icon class="detail-icon" name="people-outline"></ion-icon>
            <span>{restaurantInfo.selectedGuest} Guests</span>
          </li>
        </ul>
        <button
          className="btn btn--search btn--overlay "
        >
          Give us a rating
        </button>
      </div>
    </div>
  );
}
