import React, { useState, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import Detail from "../../components/detail/Detail";
// import slotsData from "../../mock_data/slots.json";
import { getCurrentDay } from "../../helpers/bookingHelpers";
import { getCurrentDate } from "../../helpers/bookingHelpers";
import Navbar from "../../components/navbar/Navbar";
import axios from "axios";

const Booking = () => {
  const { state } = useLocation();
  const restaurant = state.restaurant;

  const [availableSlots, setAvailableSlots] = useState([]);
  const [selectedRestaurant, setSelectedRestaurant] = useState(restaurant);
  const [selectedDay, setSelectedDay] = useState(
    state.selectedDay ? state.selectedDay : null
  );
  const [selectedDate, setSelectedDate] = useState(
    state.selectedDate ? state.selectedDate : null
  );
  const [selectedStartTime, setSelectedStartTime] = useState(
    state.selectedStartTime ? state.selectedStartTime : null
  );
  const [selectedEndTime, setSelectedEndTime] = useState(
    state.selectedEndTime ? state.selectedEndTime : null
  );
  // const [selectedEndTime, setSelectedEndTime] = useState(null);
  const [selectedGuest, setSelectedGuest] = useState(
    state.selectedGuest ? state.selectedGuest : 2
  );

  const [bookingCounts, setBookingCounts] = useState({});
  const [selectedSlot, setSelectedSlot] = useState(state || {});

  // console.log(state);

  useEffect(() => {
    // const handleSubmit = async (e) => {
    //   e.preventDefault();
    const token = localStorage.getItem("authToken");
    const fetchData = async () => {
      try {
        const res = await axios.get(
          "http://localhost:8082/slots/slots",
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          },
          {
            withCredentials: true,
          }
        );
        if (selectedRestaurant) {
          // Filter slots based on the selected restaurant id
          const filteredSlots = res.data.slots.filter(
            (slot) => slot.restaurant_id === selectedRestaurant.id
          );
          setAvailableSlots(filteredSlots);

          if (
            filteredSlots.length > 0 &&
            !selectedStartTime &&
            !selectedEndTime
          ) {
            // Set selectedStartTime and selectedEndTime after slots are updated
            setSelectedStartTime(filteredSlots[0].start_time);
            setSelectedEndTime(filteredSlots[0].end_time);
          }
        }
        console.log(res);
      } catch (err) {
        console.error(err.response.data);
      }
    };
    fetchData();
  }, []);

  // useEffect(() => {
  //   // console.log("HOLS");
  //   const fetchAvailableSlots = () => {
  //     if (selectedRestaurant) {
  //       // Filter slots based on the selected restaurant id
  //       const filteredSlots = slotsData.slots.filter(
  //         (slot) => slot.restaurant_id === selectedRestaurant.id
  //       );
  //       setAvailableSlots(filteredSlots);

  //       if (
  //         filteredSlots.length > 0 &&
  //         !selectedStartTime &&
  //         !selectedEndTime
  //       ) {
  //         // Set selectedStartTime and selectedEndTime after slots are updated
  //         setSelectedStartTime(filteredSlots[0].start_time);
  //         setSelectedEndTime(filteredSlots[0].end_time);
  //       }
  //     }
  //   };

  //   fetchAvailableSlots();
  // }, [selectedRestaurant, selectedStartTime, selectedEndTime]);

  useEffect(() => {
    if (!selectedDay && !selectedDate) {
      getCurrentDay(setSelectedDay);
      getCurrentDate(setSelectedDate);
    }
  }, [selectedDay, selectedDate]);

  return (
    <div className="details-page">
      {selectedRestaurant ? (
        <Detail
          selectedRestaurant={selectedRestaurant}
          availableSlots={availableSlots}
          selectedDay={selectedDay}
          setSelectedDay={setSelectedDay}
          selectedDate={selectedDate}
          setSelectedDate={setSelectedDate}
          selectedStartTime={selectedStartTime}
          setSelectedStartTime={setSelectedStartTime}
          selectedEndTime={selectedEndTime}
          setSelectedEndTime={setSelectedEndTime}
          selectedGuest={selectedGuest}
          setSelectedGuest={setSelectedGuest}
          bookingCounts={bookingCounts}
          setBookingCounts={setBookingCounts}
          selectedSlot={selectedSlot}
          setSelectedSlot={setSelectedSlot}
        />
      ) : (
        <p>Loading...</p>
      )}
    </div>
  );
};

export default Booking;
