import React from "react";
import Confirm from "../../components/confirm/Confirm";
import { useLocation } from "react-router-dom";

export default function Confirmation() {
  const location = useLocation();
  const { state: restaurantInfo } = location;
  console.log(restaurantInfo);

  return <Confirm restaurantInfo={restaurantInfo} />;
}
