import React from "react";
import Summary from "../../components/summary/Summary";
import { useNavigate, useLocation } from "react-router-dom";

export default function Checkout() {
  const location = useLocation();
  const state = location.state;

  // console.log(state)

  return (
    <>
      <Summary restaurantInfo={state} />
    </>
  );
}
