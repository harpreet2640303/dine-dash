import React from "react";
import SignupForm from "../../components/signupForm/SignupForm.jsx";

export default function Signup() {
  return <SignupForm />;
}
