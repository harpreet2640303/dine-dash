import Navbar from "../../components/navbar/Navbar";
import Restaurant from "../../components/restaurant/Restaurant";
import axios from "axios";
import { useState, useEffect } from "react";
import { getFilteredRestaurants } from "../../helpers/restaurantHelpers";

const Home = () => {
  const [completeRestaurants, setCompleteRestaurants] = useState([]); // New state for complete list
  const [restaurants, setRestaurants] = useState([]);
  const [searchText, setSearchText] = useState("");

  useEffect(() => {
    const fetchRestaurants = async () => {
      try {
        const response = await axios.get("http://localhost:8082/restaurants", {
          withCredentials: true,
        });
        const apiRestaurants = response.data.restaurants;

        setCompleteRestaurants(apiRestaurants);
        setRestaurants(apiRestaurants);
      } catch (error) {
        console.error("Error fetching restaurants:", error);
      }
    };

    fetchRestaurants();
  }, []);

  useEffect(() => {
    if (completeRestaurants.length > 0) {
      const updatedRestaurants = getFilteredRestaurants(
        searchText,
        completeRestaurants
      );
      setRestaurants(updatedRestaurants);
    }
  }, [searchText, completeRestaurants]);

  const restaurantComponents =
    restaurants && restaurants.length > 0
      ? restaurants.map((item) => <Restaurant key={item.id} item={item} />)
      : null;

  return (
    <div>
      <Navbar setSearchText={setSearchText} />
      <section className="restaurants-list">
        {completeRestaurants.length > 0 ? (
          restaurantComponents
        ) : (
          <p>Loading...</p>
        )}
      </section>
    </div>
  );
};

export default Home;
