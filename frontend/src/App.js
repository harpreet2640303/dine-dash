import React from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Home from "./pages/home/Home.jsx";
import Booking from "./pages/booking/Booking.jsx";
import BookingSummary from "./pages/bookingSummary/BookingSummary.jsx";
import Login from "./pages/login/Login.jsx";
import Signup from "./pages/signup/Signup.jsx";
import Confirmation from "./pages/confirmation/Confirmation.jsx";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Navigate to="/restaurant" replace />}></Route>
        <Route path="/restaurant" element={<Home />}></Route>
        {/* <Route path="/booking" element={<Booking />}></Route> */}
        <Route path="/restaurant/:id" element={<Booking />}></Route>
        <Route path="/booking" element={<BookingSummary />}></Route>
        <Route path="/login" element={<Login />}></Route>
        <Route path="/signup" element={<Signup />}></Route>
        <Route path="/confirmation" element={<Confirmation />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
