export const getFilteredRestaurants = (searchItem, data) =>
  data.filter((restaurant) => {
    const nameMatch = restaurant.name
      .toLowerCase()
      .includes(searchItem.toLowerCase());
    const locationMatch = restaurant.location
      .toLowerCase()
      .includes(searchItem.toLowerCase());
    const cuisineMatch = restaurant.cuisine_type
      .toLowerCase()
      .includes(searchItem.toLowerCase());

    return nameMatch || locationMatch || cuisineMatch;
  });
