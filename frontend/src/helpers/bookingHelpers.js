export function getCurrentDay(setSelectedDay) {
  const daysOfWeek = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  const today = new Date();
  const tomorrow = new Date(today);
  tomorrow.setDate(today.getDate() + 1);

  const dayIndex = today.getDay();

  const isToday = (date) =>
    date.getDate() === today.getDate() &&
    date.getMonth() === today.getMonth() &&
    date.getFullYear() === today.getFullYear();

  if (isToday(today)) {
    setSelectedDay("Today");
  } else if (isToday(tomorrow)) {
    setSelectedDay("Tomorrow");
  } else {
    setSelectedDay(daysOfWeek[dayIndex]);
  }
}

export function getCurrentDate(setSelectedDate) {
  const monthsAbbreviation = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];

  const today = new Date();
  // const day = today.getDate();
  const day = today.getDate().toString().padStart(2, '0');
  const monthIndex = today.getMonth();
  const formattedDate = `${day}-${monthsAbbreviation[monthIndex]}`;

  setSelectedDate(formattedDate);
}
