const Data = [
  {
    day: [
      "Today",
      "Tomorrow",
      "Thursday",
      "Friday",
      "Saturday",
      "Monday",
      "Tuesday",
    ],

    date: ["24-Jan", "25-Jan", "25-Jan", "25-Jan", "25-Jan", "25-Jan"],

    diningTime: ["6:30 PM", "7:00 PM", "7:30PM", "8:00PM"],

    guests: [1, 2, 3, 4, 5],
  },
];

// const data = [
//   {
//     da
//   }
// ]

export default Data;
