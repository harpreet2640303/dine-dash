const Data = [
  {
    key: 1,
    title: "Radhuny",
    location: "Hamirpur",
    googleMapsUrl: "https://goo.gl/maps/1DGM5WrWnATgkSNB8",
    cuisine_type: "Chinese",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    imageUrl:
      "https://source.unsplash.com/photo-of-pub-set-in-room-during-daytime-poI7DelFiVA",
  },
  {
    key: 2,
    title: "The Raja",
    location: "Noida",
    googleMapsUrl: "https://goo.gl/maps/681n4jdijgdt3uGS6",
    cuisine_type: "Chinese",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    imageUrl:
      "https://source.unsplash.com/rectangular-beige-wooden-tables-and-chair-Ciqxn7FE4vE",
  },
  {
    key: 3,
    title: "Albert Spice",
    location: "Delhi",
    googleMapsUrl: "https://goo.gl/maps/5J6KAPeSgM8CHQwB7",
    cuisine_type: "Indian",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    imageUrl:
      "https://source.unsplash.com/brown-and-gray-concrete-store-nmpW_WwwVSc",
  },
  {
    key: 4,
    title: "Albert Spice",
    location: "Solan",
    googleMapsUrl: "https://goo.gl/maps/5J6KAPeSgM8CHQwB7",
    cuisine_type: "Indian",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    imageUrl:
      "https://source.unsplash.com/bottles-in-display-cabinet-B3Jt_d5E2Lk",
  },
  {
    key: 5,
    title: "Polkash Indian Takeaway",
    location: "Gurgaon",
    googleMapsUrl: "https://goo.gl/maps/5J6KAPeSgM8CHQwB7",
    cuisine_type: "Indian",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    imageUrl:
      "https://source.unsplash.com/rectangular-beige-wooden-tables-and-chair-Ciqxn7FE4vE",
  },
];

// const Data = [
//   {
//     key: 1,
//     title: "Radhuny",
//     location: "Hamirpur",
//     googleMapsUrl: "https://goo.gl/maps/1DGM5WrWnATgkSNB8",
//     cuisine_type: "Indian",
//     imageUrl:
//       "https://source.unsplash.com/photo-of-pub-set-in-room-during-daytime-poI7DelFiVA",
//   },
//   {
//     key: 2,
//     title: "The Raja",
//     location: "Noida",
//     googleMapsUrl: "https://goo.gl/maps/681n4jdijgdt3uGS6",
//     cuisine_type: "Indian",
//     imageUrl:
//       "https://source.unsplash.com/rectangular-beige-wooden-tables-and-chair-Ciqxn7FE4vE",
//   },
//   {
//     key: 3,
//     title: "Albert Spice",
//     location: "Delhi",
//     googleMapsUrl: "https://goo.gl/maps/5J6KAPeSgM8CHQwB7",
//     cuisine_type: "Indian",
//     imageUrl:
//       "https://source.unsplash.com/brown-and-gray-concrete-store-nmpW_WwwVSc",
//   },
//   {
//     key: 3,
//     title: "Albert Spice",
//     location: "Solan",
//     googleMapsUrl: "https://goo.gl/maps/5J6KAPeSgM8CHQwB7",
//     cuisine_type: "Indian",
//     imageUrl:
//       "https://source.unsplash.com/bottles-in-display-cabinet-B3Jt_d5E2Lk",
//   },
//   {
//     key: 3,
//     title: "Polkash Indian Takeaway",
//     location: "Gurgaon",
//     googleMapsUrl: "https://goo.gl/maps/5J6KAPeSgM8CHQwB7",
//     cuisine_type: "Indian",
//     imageUrl:
//       "https://source.unsplash.com/rectangular-beige-wooden-tables-and-chair-Ciqxn7FE4vE",
//   },
// ];

export default Data;
